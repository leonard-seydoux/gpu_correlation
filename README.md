# Using TensorFlow for GPU-accelerated seismic data analysis

by Léonard Seydoux (leonard.seydoux@univ-grenoble-alpes.fr)


## Content

This repository contains a [notebook](xcorr_with_tf.ipynb) that shows how to use tensorflow as a GPU-accelerated signal processing library, with an example of application on seismic cross-correlation. Please, feel free to copy and modify this code for your case. Some potential application in geophysics are mentionned in the [sides](slides.html) available in html format.


## Data download

The dataset I used for the example is large (> 3GB), so I did not push it on the current repository. You can download the very same dataset from IRIS with the folloowing command (it involves installing the `obspyDMT` soft with `conda` or `pip`):

```
obspyDMT --continuous --datapath sac --min_date 2010-01-01 --max_date 2010-02-01 --net G --cha BHZ --sampling_rate 2 --waveform_format sac
```

## Additional notes

The slides have been generated with [Marp](https://marp.app), a simple framework based on Markdown.
