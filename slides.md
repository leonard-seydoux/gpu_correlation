---
marp: true
paginate: true
---

<style>
section {background: #f6f6f6; height: 720px;}
/* * {margin: auto; text-align: center; font-family: SF Pro Display} */
li{text-align: left;}
h1 {color: black; font-weight: normal; text-align: left; margin-bottom: auto; font-size:larger;}
h1 strong{font-weight: 700; color: inherit;}
h2{color: #e0671b; font-weight: normal; text-align: left; margin-top: auto;font-size:100% !important;}
h2 strong{font-weight: 700; color: inherit;}
h3{color: grey;font-weight: normal;text-align: left;margin: auto; font-size: 80%;}
table{font-size:70%; border: 1px solid red; display: inline-table; max-width: 800px;}
td{text-align: left;font-size:larger;}
th{text-align: left;}
</style>

<!-- 
_backgroundColor: #e6da81
_paginate: false
style: |
  h3 {color: white !important;}
  img {background-color: transparent;}
-->

# Using TensorFlow for <br> GPU-accelerated seismic data analysis


![bg right:40% width:100%](images/tpu-google.png)


## Léonard Seydoux <br> ISTerre, Univeristé Grenoble Alpes <br><br> `leonard.seydoux@univ-grenoble-alpes.fr`



---

# GPU-based scientific calculation

- What are CPU/GPU/TPU?
- How can we use them?
- Quick example with cross-correlation

![bg right:40% width 100%](images/tpu-google.png)

## I _try_ to also answer your questions 😇

---

# About the hardware


|CPU|GPU|TPU|
|-|-|-|
| Central Processing Unit | Graphics Processing Unit | Tensor Processing Unit |
| Wide range of tasks | Specific tasks | AI-specific |
| Few cores | Thousands of cores | Even more |
| Serial processing | Parallel processing | Massive data parallel |

## GPU perform better for _parallelization_

---

# Python and GPU calculation

Python is executed on CPU in a serial way (line by line),
but some operations can be GPU-accelerated:

- Matrix multiplication
- Convolution
- Fourier transform...

## 

---

# GPU-accelerated Python libraries

![bg right:45% width:80%](images/cupy.png)
<!-- _footer: source of CuPy vs. Numpy: https://cupy.chainer.org -->

- __CuPy__: numpy in CUDA
- __Jax__: numpy in XLA
- __Numba__: compile Python in CUDA

## These libraries can really speed up computations (and trigger headaches)


---

<!-- 
_backgroundColor: #e6da81
_paginate: false
style: |
  h3 {color: white !important;}
  img {background-color: transparent;}
-->

# Today's example with TensorFlow

![bg right:40% width:30%](images/Tensorflow_logo.svg)

<br> 

- Open-source deep-learning library
- Released by __Google Brain__
- Can be used in imperative mode (v2)

## 

---

# TensorFlow is symbolic

<br>

![bg right:40% width:30%](images/Tensorflow_logo.svg)

```python 
import tensorflow as tf

# Variables to be learned
W = tf.Variable(tf.ones(shape=(2,2)))
b = tf.Variable(tf.zeros(shape=(2)))

# Model
@tf.function
def forward(x):
  return W * x + b

# Prediction
out = forward([1,0])
```
<br>

## This can be __any geophysical model__ 


---

# Applications where TensorFlow could be really cool

<br> 

- __Massive data processing__ (detection, xcorr...)
- __Massive data vizualization__ (dimension reduction)
- __Geophysical model inversion__ (tomography, localization...)
- And also... __deep learning applications!__

![bg right:50% width:80%](images/insar.jpg)

## 

<!-- _footer: -->

---

# Today' exemple: cross-correlation with continuous seismic data

<br>

![width:800px](images/axes.svg)

<br>

## →  Switch to notebook
